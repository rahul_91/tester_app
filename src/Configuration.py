'''
Created on Jun 14, 2019

@author: ubuntu
sudo apt-get install python-configobj

'''
import logging


logger = logging.getLogger(__name__)
class ConfigHelper(object):
    '''
    classdocs
    '''


    def __init__(self, arg,start_time):
        '''
        Constructor
        '''
        self.arg = arg
        self.cmd_parameter = {'-d':1,
                         '-s':None,
                         '-node_ip':None,
                         '-tpid':None,
                         '-b':100,
                         '-graph_mode':'regression',
                         'start_time' : start_time
                        }
        
        
        
    def upload_config(self):
        logger.info("Updating command line argument")
        for index in range(1,len(self.arg),2):
            self.cmd_parameter[self.arg[index]]= self.arg[index+1]
            
        if self.cmd_parameter['-s'] is None:
            raise Exception('please provide service name in command . \n  Ex= -s servicename ')
        if self.cmd_parameter['-node_ip'] is None:
            raise Exception('please provide node ip in command . \n  Ex= -node_ip 192.168.100.1 ')
        if self.cmd_parameter['-tpid'] is None:
            raise Exception('please provide tp id in command . \n  Ex= -tpid 123 ')
        return self.cmd_parameter
         
        