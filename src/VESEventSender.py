#!/usr/bin/python3.5
'''
Created on Jun 7, 2019

@author: Rahul Tamrkar
'''

import uuid
import json
import requests
import time
import csv
import os
import logging
from conf.property_holder import get_value
logger = logging.getLogger(__name__)


class VESEventSender(object):
    
    def run_script(self,bandwidth,node_ip,time_in_sec):
        '''
            Running iperf script to generate traffic over the network
        '''
        try:
            logger.info("Executing system command ")
            myCmd = 'iperf3 -c '+get_value('server_ip')+' -p '+get_value('server_port')+' -t '+time_in_sec+' -b '+str(bandwidth)+'m'
            os.system(myCmd)
        except Exception as e:
            logger.error("Exception message :: %s",str(str(e)))
            
            
            
    def update_jsonfile(self,uuid,bandwidth,duration,details):
        '''
            Update uuid , bandwidth and time in vesevent.json file
    
        '''
        logger.info("Updating new event info in vesevent.json file")
        self.run_script(bandwidth, details['-node_ip'],str(details['-d']))
        jsonFile = open("/home/ubuntu/shell_proj/vesevent_listner/src/vesevent.json", "r")
        data = json.load(jsonFile)
        data["event"]["commonEventHeader"]["eventId"] = uuid
        data["event"]["faultFields"]["alarmAdditionalInformation"][1]["value"] = str(bandwidth)
        sec = int(round(time.time() * 1000))/1000
        self.output_in_file(bandwidth,duration)
        data["event"]["faultFields"]["alarmAdditionalInformation"][2]["value"] = str(duration)
        data["event"]["faultFields"]["alarmAdditionalInformation"][3]["value"] = str(details['-node_ip']) # node ip
        data["event"]["faultFields"]["alarmAdditionalInformation"][4]["value"] = str(details['-tpid']) # tp id
        data["event"]["faultFields"]["alarmAdditionalInformation"][5]["value"] = str(details['-s']) # service name
        ## Save our changes to JSON file
        jsonFile = open("vesevent.json", "w+")
        jsonFile.write(json.dumps(data))
        jsonFile.close()




    def call_request(self):
        '''
           Calling ves collector api to update new uuid, bandwidth and time.

        '''
        try: 
            if get_value('REPORT_VES_AND_AAI') == 'yes':
                contents = open('vesevent.json', 'rb').read()
                headers = {'Accept' : 'application/json', 'Content-Type' : 'application/json'}
                url = "http://"+str(get_value('VES_IP'))+':'+str(get_value('VES_PORT'))+"/eventListener/v5"
                r = requests.post(url =url, data=contents, headers=headers)
                logger.info("Ves endpoint response code = %s",str(r.status_code))
        except Exception as e:
            logger.error("Exception msg :: %s",str(e)) 
            #raise Exception(str(e))




    def generate_uuid(self):
        '''
            Generate uniqe no for event id column.
        '''
        ui= str(uuid.uuid1())
        ui = ui.replace("-","")
        logger.info("Generated uuid is %s",str(ui))
        return ui




    def output_in_file(self,bandwidth,duration):
        ''' 
           Writting time duration and bandwidth in csv file.   
        '''
        logger.info("Writing data intio file")
        newRow = [str(duration),str(bandwidth)]
        with open('output_test1.csv', 'a+') as csvfile:
            csvwriter = csv.writer(csvfile, delimiter=',',quotechar='|', quoting=csv.QUOTE_MINIMAL)
            csvwriter.writerow(newRow)




    def get_operational_status(self,service_details):
        '''
            Calling aai to get the operation status of particular node_ip
        '''
        logger.info("Calling aai service end point")
        url = 'http://'+str(get_value('AAI_IP'))+':'+str(get_value('AAI_PORT'))+'/api/aai-network/v16/pnfs/pnf/'+str(service_details['-node_ip'])+'/p-interfaces/p-interface/nodeId-'+str(service_details['-node_ip'])+'-ltpId-'+str(service_details['-tpid'])
        
        print("this is aai url = "+url)
        #exit(0)
        headers = {'Accept' : 'application/json', 
                           'X-FromAppId': 'jimmy-postman', 
                           'X-TransactionId': '9999',
                           'Authorization': 'Basic QUFJOkFBSQ=='}
        try:
            logger.info("aai service end point = %s",str(url))
            if get_value('REPORT_VES_AND_AAI') == 'yes':
                r = requests.get(url,headers=headers,verify= True)
                logger.info("aai servicde endpoint response code = %s  , status code= %s",str(r.status_code),str(r.json()['operational-status']))
                return str(r.json()['operational-status']).lower()
            return "Down".lower()
        except Exception as e:
            logger.error("Error occured while calling aai end point")
            logger.error("Error message : %s",str(e))
            print(str(e))


