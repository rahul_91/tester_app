'''
Created on Jun 19, 2019

@author: ubuntu
'''



from configobj import ConfigObj
import datetime
import logging

logger = logging.getLogger(__name__)
def get_value(key):
    logger.info("Reading configuration file")
    config = ConfigObj("config.conf")
    value = config[key]
    config.clear()
    return value;


#print(get_value('sinus_duration'))


def get_endtime():
    end_time = datetime.datetime.now().time().strftime('%H:%M:%S')
    return end_time


def start_time():
    start_time = datetime.datetime.now().time().strftime('%H:%M:%S')
    return start_time

