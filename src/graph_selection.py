'''
Created on Jun 12, 2019

@author: ubuntu
'''


from VESEventSender import VESEventSender
from pattern_picker import Pattern
import numpy as np
import datetime
from random import randint, random
import logging
from conf.property_holder import get_value, get_endtime




logger = logging.getLogger(__name__)
def bandwidth_validation(bandwidth,duration,details):
    logger.info("Validating bandwidth")
    total_time=(datetime.datetime.strptime(get_endtime(),'%H:%M:%S') - datetime.datetime.strptime(details['start_time'],'%H:%M:%S'))
    print("Bandwidth = {}".format(str(bandwidth)))
    print ('time diff= {} '.format(str(total_time)))
    ves = VESEventSender()
    #print("operational status from aai  "+str(ves.get_operational_status()))
    if ves.get_operational_status(details) == 'overloaded':
    #if 'overloaded' == 'overloaded':   
        while True:
            duration = duration + int(details['-d'])
            if int(get_value('traffic_limit')) == 2 or int(get_value('traffic_limit')) != 1:
                send_event(2, duration,details)
            elif int(get_value('traffic_limit')) ==1:
                send_event(1024, duration,details)
            raise Exception('got overloaded ststau from aai is overloaded')
    if bandwidth >=1024 or bandwidth == 2:
        currentDT = datetime.datetime.now()
        print (str(currentDT))
        if bandwidth != 2:
            bandwidth = bandwidth - (bandwidth-1024)
        duration = duration + int(details['-d'])
        send_event(bandwidth, duration,details)
        if int(get_value('traffic_limit')) == 2 or int(get_value('traffic_limit')) != 1:
            bandwidth =2 
            duration = duration + int(details['-d'])
            call_veslistner(bandwidth, duration,details)
            total_time=(datetime.datetime.strptime(get_endtime(),'%H:%M:%S') - datetime.datetime.strptime(details['start_time'],'%H:%M:%S'))
            print("Bandwidth = {}".format(str(bandwidth)))
            print ('time diff= {} '.format(str(total_time)))
                
            #raise Exception('Bandwidth exceeded {}'.format(bandwidth))
        elif int(get_value('traffic_limit')) ==1:
            bandwidth =1024
            duration = duration + int(details['-d'])
            call_veslistner(bandwidth, duration,details)
            total_time=(datetime.datetime.strptime(get_endtime(),'%H:%M:%S') - datetime.datetime.strptime(details['start_time'],'%H:%M:%S'))
    return




def zigzag_motion(start,end,wait,sec,config_details):
    logger.info("Executing zigzag motion")
    start_val = end
    counter =1
    while True:
        sec = sec + wait
        if (start_val <= start) or (start_val <= start+2):
            break
        if counter ==1:
            start = start +2
            call_veslistner(start, sec,config_details)
            counter = counter -1
            continue
        start = start -1
        call_veslistner(start, sec,config_details)
        counter = counter +1
    return sec
    
    
    
# Currnt execution will select the graph from listed down 
def regression_graph(duration,base_value,config_details):
    '''
        This script will generate regression graph
    '''
    logger.info("Execution started for regression graph")
    d_inc = duration
    print("base_val ",base_value)
    pattObj = Pattern(base_value)
    pattern = pattObj.pick_pattern()
    print(pattern)
    iteration = 1
    #   no = 0 
    while True:
        iteration = iteration + 1
        for bwidth in range(0,12):
            if(bwidth == 0):
                base_value = base_value + pattern[0]
                call_veslistner(base_value, duration,config_details)
                duration = duration + d_inc
            elif(bwidth == 1):
                base_value = base_value - pattern[1] # - 10
                call_veslistner(base_value, duration,config_details)
                duration = duration + d_inc
            elif(bwidth == 2):
                base_value = base_value + pattern[2] # - 10
                call_veslistner(base_value, duration,config_details)
                duration = duration + d_inc
            elif(bwidth == 3):
                base_value = base_value - pattern[3] # - 10
                call_veslistner(base_value, duration,config_details)
                duration = duration + d_inc
            elif(bwidth == 4):
                base_value = base_value + pattern[4]
                call_veslistner(base_value, duration,config_details)
                duration = duration + d_inc
            elif(bwidth == 5):
                base_value = base_value - pattern[5] # - 10
                call_veslistner(base_value, duration,config_details)
                duration = duration + d_inc
            elif(bwidth == 6):
                base_value = base_value + pattern[6] # + 10
                call_veslistner(base_value, duration,config_details)
                duration = duration + d_inc
            elif(bwidth == 7):
                base_value = base_value - pattern[7] # -10
                call_veslistner(base_value, duration,config_details)
                duration = duration + d_inc
            elif(bwidth == 8):
                base_value = base_value + pattern[8] # + 30
                call_veslistner(base_value, duration,config_details)
                duration = duration + d_inc
            elif(bwidth == 9):
                base_value = base_value - pattern[9] # -15
                call_veslistner(base_value, duration,config_details)
                duration = duration + d_inc
            elif(bwidth == 10):
                base_value = base_value + pattern[10] # -15
                call_veslistner(base_value, duration,config_details)
                duration = duration + d_inc
            elif(bwidth == 11):
                base_value = base_value - pattern[11] # -15
                call_veslistner(base_value, duration,config_details)
                duration = duration + d_inc
                
    

def curve_graph(duration,details):
    '''
        * To generate sinusoidal graph.
        * In this case bandwidth comming from command line will not be considered
          As to generate sinusoidal graph , we are using matplotlib and numpy library.
    '''
    logger.info("Execution started for curve graph")
    index_inc = int(get_value('skip_val'))
    time_in_sec = duration
    x_axis = np.array(range(1000))
    noise = np.random.uniform(-0.2, 0.2, 1000)
    y = np.sin(np.pi * x_axis / 100) + x_axis / 200. + noise
    lis=[]
    for no in y:
        lis.append(int(no*200))
    logger.info("Generated bandwidth list size is %s",str(len(lis)))
    for index in range(min(x_axis),max(x_axis),index_inc):
        band_val = int(lis[index])
        if band_val == 2:
            band_val = 3
        if band_val < 0:
            band_val = randint(5,12)
        call_veslistner(band_val, duration,details) 
        duration = duration + time_in_sec



def linear_graph(duration,details):
    '''
        To generate linear graph
    '''
    logger.info("Executing started for linear graph")
    bandwidth = int(details['-b'])
    pattern_filter = int(bandwidth/2)
    #diff = int(bandwidth/2)
    #ban= bandwidth
    increment = duration
    #case =0 
    while True:
        if bandwidth == 2:
            bandwidth = 3
        call_veslistner(bandwidth, duration, details)
        bandwidth = bandwidth+pattern_filter
        duration = duration+increment
            
            
            
        
        '''if case==4:
            val = bandwidth+randint(-bandwidth,bandwidth)
            call_veslistner(val, duration, details)
            bandwidth = bandwidth+pattern_filter
            duration = duration+increment
            case = 0
        else:
            call_veslistner(bandwidth, duration, details)
            bandwidth = bandwidth+pattern_filter
            duration = duration+increment
            case = case + 1'''
#         diff = int(bandwidth/2)
#         call_veslistner(bandwidth, duration, details)
#         bandwidth = bandwidth+pattern_filter
        
#         call_veslistner(bandwidth+randint(-(pattern_filter),pattern_filter), duration, details)
#         bandwidth = bandwidth+pattern_filter
#         duration = duration + increment
#         val = bandwidth+randint(-ban,ban)
#         if case == 3:
#             call_veslistner(val, duration, details)
#             duration = duration + increment
#             bandwidth = bandwidth  + pattern_filter
#  #           case = 0
# #         if case == 7:
# #             call_veslistner(bandwidth+randint(pattern_filter,bandwidth), duration, details)
# #             duration = duration + increment
# #             bandwidth = bandwidth  + pattern_filter
# #             case = 0
#         else:
#             call_veslistner(bandwidth, duration, details)
#             duration = duration + increment
#             bandwidth = bandwidth  + pattern_filter
#             case = case+1
            
        
# ----------end of the graph ------------




# Calling ves collector api written in class VESEventSender
def call_veslistner(bandwidth,duration,details):
    logger.info("Executing call_veslistner")
    bandwidth_validation(bandwidth,duration,details)
    send_event(bandwidth, duration,details)
    

def send_event(bandwidth,duration,details):
    logger.info("Calling VES collector Module")
    ves = VESEventSender()
    ves.update_jsonfile(ves.generate_uuid(),bandwidth,duration,details)
    ves.call_request()
    
    
    