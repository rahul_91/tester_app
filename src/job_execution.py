'''
Created on Jun 12, 2019

@author: ubuntu

    
    python file.py -d 120 -s ser1 -ip 19299299 -b 100 -tpid 123 -graph_mode linear
               0    1  2   3   4    5   6       7   8    9   10   11          12
                    ['-d','-s','-ip','']
    1. Starting point of execution.
    2. Pass time duration and bandwidth from command line 
       Ex :- python vesevent_caller.py 5 20 
    3. First argument for time(in sec)
    4. Sec argument for bandwidth in mb
    5. In case you are not passing time and bandwidth then Default value for time 5sec and bandwidth 20mb
    
'''

import sys
from enum.mode.GraphMode import enum
from Configuration import ConfigHelper
from graph_selection import call_veslistner, regression_graph, curve_graph,\
    linear_graph
import datetime
from log.config import config_log
import logging

logger = logging.getLogger(__name__) 
def usage():
    logger.info("Documenation called")
    print('''
Documentation:-
-------------
    Use the below flags to set the parameters in command 
        -d  : To set the time interval for current execution
        -s  : Set service name for the current execution (mandatory)
        -node_ip  : Set node ip  (mandatory)
        -tpid  : Set tpid for the current execution (mandatory)
        -graph_mode : Set the graph mode(regression,curve,linear)
        -b  : Set the base value for bandwidth
        
Ex:-  
    Sinusoidal Graph :- python job_execution.py -d 1 -s serv6 -node_ip 192.168.100.1 -tpid 1234 -graph_mode curve -b 100
    ----------------
    Linear Graph:- python job_execution.py -d 1 -s serv6 -node_ip 192.168.100.1 -tpid 1234 -graph_mode regression -b 80
    ------------
    Regression Graph:- python job_execution.py -d 1 -s serv6 -node_ip 192.168.100.1 -tpid 1234 -graph_mode linear -b 100
    ----------------
        
Note:- 1. Mandatory fields should be filled to execute the script
       2. If you'll not pass fields which is not mandatory. in that case the script will pick default value given below
            -d 1
            -b 150
            -graph_mode curve
    ''')
    

if __name__ == '__main__':
    config_log()
    if len(sys.argv) < 2:
        logger.warn("Please pass atlease mandatory argument in command line")
        usage()
        exit(0)
    logger.info("Execution started")
    currentDT = datetime.datetime.now()
    print (str(currentDT))
    graph_mode = enum(REGRESSION='regression',CURVE='curve',LINEAR='linear')
    start_time = datetime.datetime.now().time().strftime('%H:%M:%S')
    conf = ConfigHelper(sys.argv,start_time)
   
    try:
        details = conf.upload_config()
        print("before if")
        
        logger.info("Selected %s graph",str(details['-graph_mode']))
        if graph_mode.REGRESSION == details['-graph_mode']:
            #print(details['-graph_mode'])
            call_veslistner(int(details['-b']), 0,details)
            regression_graph(int(details['-d']),int(details['-b']),details)
        elif graph_mode.CURVE == details['-graph_mode']:
            #print(details['-graph_mode'])
            print(details)
            curve_graph(int(details['-d']),details)
        elif graph_mode.LINEAR == details['-graph_mode']:
            print(details['-graph_mode'])
            print(details)
            linear_graph(int(details['-d']),details)
    except Exception as e:
        logger.error("Error message %s",str(e))
        end_time = datetime.datetime.now().time().strftime('%H:%M:%S')
        total_time=(datetime.datetime.strptime(end_time,'%H:%M:%S') - datetime.datetime.strptime(start_time,'%H:%M:%S'))
        print(total_time)
        print(str(e)) 
            
            
            