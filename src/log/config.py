'''
Created on Jun 18, 2019

@author: ubuntu
'''

import logging

def config_log():
    logging.basicConfig(filename='testapp.log', level=logging.INFO,format='%(asctime)s :: %(levelname)s :: %(module)s :: %(message)s')
    
    logging.info('Started')