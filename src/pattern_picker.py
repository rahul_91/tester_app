
'''
Created on Jun 12, 2019

@author: ubuntu
'''

class Pattern(object):
    '''
    classdocs
    '''

    def __init__(self,bandwidth):
        '''
        Constructor
        '''
        self.bandwidth = bandwidth
    
    def pick_pattern(self):
        if self.bandwidth <= 100:
            return [20,10,20,10 ,20,10,10,10,110,90,70,50]
        if self.bandwidth > 100 and self.bandwidth <= 160:
            return [40,20,40,20 ,40,20,20,20,150,90,120,100]
        
        #if self.bandwidth > 100 and self.bandwidth <= 160:
        #   return [60,30,60,30 ,60,30,30,30,150,100,130,110]
#         if self.bandwidth > 160 and self.bandwidth <= 240:
#             return [40,20,40,20 ,40,20,20,20,60,20,40,50]
#         return [40,20,40,20 ,40,20,20,20,60,20,40,20,40]